#!/bin/bash

iptables --line-numbers -nL

echo "Informe o número da regra que deseja excluir: "
read REGRA
echo " "

iptables -D INPUT $REGRA

echo "Excluindo a regra de número: $REGRA..."
sleep 2

echo "Regra excluída!!!"
echo " "

iptables --line-numbers -nL

sleep 2

clear
