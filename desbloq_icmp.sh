#!/bin/bash

echo "Informe qual a porta deseja abrir: "
read PORTA
echo " "

echo "Informe o IP que deseja ter acesso: "
read IP
echo " "

echo "Liberando a porta $PORTA para o IP: $IP "

sleep 2

iptables -I INPUT -p $PORTA -s $IP -j ACCEPT

echo " "

echo "Porta $PORTA liberada para o IP $IP!"

sleep 2
