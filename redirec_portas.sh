#!/bin/bash

echo "Qual a porta que deseja redirecionar? "
read PORTA
if [ "$PORTA" = 80 ]
then
	echo "Aplicando uma das regras práticas de segurança padrão..."
	echo "Redirecionando a porta HTTP para a porta HTTPS."
	sleep 3
	iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 443
	echo "Executado o redirecionamento!!"
	sleep 3

else
	echo "Favor, verifique as regras práticas de segurança padrão."
	exit

fi


